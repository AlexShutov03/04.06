import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {

        Figure[] array = new Figure[5];
        System.out.println("Все фигуры (Фигура - площадь):");
        int s;
        DecimalFormat df = new DecimalFormat("###.####");
        for (int i = 0; i < 5; i++) {
            if(Math.random() < 0.5) { //строка для заполнения массива случайными фигурами
                array[i] = new Circle((int) (Math.random() * 100), (int) (Math.random() * 100));
                s=1; // s - переменная для вывода определенной фигуры
            } else {
                array[i] = new Rectangle((int) (Math.random() * 100), (int) (Math.random() * 100));
                s=2;
            }
            switch(s) {
                case 1:
                    System.out.println("Окружность - " + df.format(array[i].square()));
                    break;
                case 2:
                    System.out.println("Прямоугольник - " + df.format(array[i].square()));
                    break;
            }
        }
    }
}