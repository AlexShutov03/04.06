public class Rectangle extends Figure {
    private final int h = (int) (Math.random() * 20); //длина
    private final int w = (int) (Math.random() * 20); //высота

    public Rectangle (int x0, int y0) {
        super(x0, y0);
    }

    @Override
    public double square () {
        return h * w;
    }
}
