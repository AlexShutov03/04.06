import java.util.Random;

public class Circle extends Figure {
    private final int radius = (int) (Math.random() * 10);

    public Circle (int x0, int y0) {
        super(x0, y0);
    }

    @Override
    public double square() {
        return Math.PI * Math.pow(radius, 2);
    }
}
