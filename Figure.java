public abstract class Figure {
    private int x0;
    private int y0;

    public Figure(int centerX, int centerY) {
        this.x0 = x0;
        this.y0 = y0;
    }

    public abstract double square();

    public int getQuadrant() {
        if (x0 > 0) {
            if (y0 > 0) {
                return 1;
            } else {
                return 4;
            }
        } else {
            if (y0 > 0) {
                return 2;
            } else {
                return 3;
            }
        }
    }
}